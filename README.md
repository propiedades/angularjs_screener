# Introduccion

Este repositorio esta basado en [TodoMVC](http://todomvc.com/).

Primero debes poder ejecutar el ejercicio en un ambiente de desarrollo local (no te olvides del `npm install`).

El ejercicio consta de varias actividades. Primero se debe corregir un bug y luego agregar algo de funcionalidad (importar/exportar).


# Corregir este bug

El filtrado por "All", "Active", y "Completed" no funciona.


# Importar/Exportar

Necesiatmos agregar la funcionalidad de importar y exportar las tareas hacia (o desde) un archivo de texto.


## Formato del archivo

Para importar o exportar debemos usar el mismo formato simple en el archivo de texto codificado com UTF-8.

Debe haber un tarea por renglón; el formato de nueva línea debe ser el de Windows (`\r\n`).

Por ejemplo, un archivo con tres tareas sería así:

```txt
tarea uno
recoger a los niños
otra "tarea"
```

## Exportar

Debe haber una manera de obtener el archivo de texto como una descarga del navegador (es decir, en navegador nos debe preguntar en donde guardar el archivo).

Al guardar el archivo este debe tener las tareas visibles ("All", "Active", o "Completed") en el momento que se inició la descarga.

## Importar

Al importar se debe abrir el dialogo de selección de archivo del navegador; al recibir el archivo, cada renglon debe agregarse como una tarea sin completar.
